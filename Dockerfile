FROM nginx:1.14.2-alpine

# USER root

# COPY nginx/default.conf /etc/nginx/conf.d/
# RUN mkdir -p /var/log/app_engine

# RUN mkdir -p /usr/share/nginx/www/_ah && \
#     echo "healthy" > /usr/share/nginx/www/_ah/health
# RUN rm -rf /usr/share/nginx/html/*

COPY src/ /usr/share/nginx/html

# CMD ["nginx", "-g", "daemon off;"]
